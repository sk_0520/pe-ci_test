using System;
using System.Collections.Generic;
using System.Text;
using ContentTypeTextNet.Pe.Bridge.Plugin;
using ContentTypeTextNet.Pe.Embedded.Attributes;

[assembly: PluginIdentifiers("Pe.Plugins.Reference.ClassicTheme", "67F0FA7D-52D3-4889-B595-BE3703B224EB")]
[assembly: PluginSupportVersions("0.0.0", "0.0.0", "https://bitbucket.org/sk_0520/pe/downloads/update-Pe.Plugins.Reference.ClassicTheme.json")]
[assembly: PluginAuthors("sk", PluginLicense.DoWhatTheF_ckYouWantToPublicLicense2, "https://content-type-text.net/", "https://bitbucket.org/sk_0520/pe/wiki/Home")]
