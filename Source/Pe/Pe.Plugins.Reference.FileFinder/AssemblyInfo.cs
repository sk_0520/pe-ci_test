using System;
using System.Collections.Generic;
using System.Text;
using ContentTypeTextNet.Pe.Bridge.Plugin;
using ContentTypeTextNet.Pe.Embedded.Attributes;

[assembly: PluginIdentifiers("Pe.Plugins.Reference.FileFinder", "9DCF441D-9F8E-494F-89C1-814678BBC42C")]
[assembly: PluginSupportVersions("0.0.0", "0.0.0", "https://bitbucket.org/sk_0520/pe/downloads/update-Pe.Plugins.Reference.FileFinder.json")]
[assembly: PluginAuthors("sk", PluginLicense.DoWhatTheF_ckYouWantToPublicLicense2, "https://content-type-text.net/", "https://bitbucket.org/sk_0520/pe/wiki/Home")]

