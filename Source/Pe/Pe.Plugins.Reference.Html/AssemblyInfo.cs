using System;
using System.Collections.Generic;
using System.Text;
using ContentTypeTextNet.Pe.Bridge.Plugin;
using ContentTypeTextNet.Pe.Embedded.Attributes;

[assembly: PluginIdentifiers("Pe.Plugins.Reference.Html", "4FA1A634-6B32-4762-8AE8-3E1CF6DF9DB1")]
[assembly: PluginSupportVersions("0.0.0", "0.0.0", "https://bitbucket.org/sk_0520/pe/downloads/update-Pe.Plugins.Reference.Html.json")]
[assembly: PluginAuthors("sk", PluginLicense.DoWhatTheF_ckYouWantToPublicLicense2, "https://content-type-text.net/", "https://bitbucket.org/sk_0520/pe/wiki/Home")]
